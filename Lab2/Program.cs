﻿using System;
using System.Collections.Generic;

namespace LAB_2_1
{

    public class Chess
    {
        public int dask = 0;
        public string Name;
        public string course;
        public void Dask()
        {
            dask++;
        }
    }


    public class ChessEvent
    {
        public List<Chess> White;
        public List<Chess> Black;
        public delegate void ChessEventHandler(object sender, ChessEventArgs e);

        public event ChessEventHandler _GetDeskWhite;
        public event ChessEventHandler _GetDeskBlack;
        public event ChessEventHandler _NamePlayersOne;
        public event ChessEventHandler _NamePlayersTwo;
        public event ChessEventHandler _DebutChess;

        public void GetDeskWhite(Chess chess)
        {
            _GetDeskWhite?.Invoke(this, new ChessEventArgs(chess));
        }

        public void GetDeskBlack(Chess chess)
        {
            _GetDeskBlack?.Invoke(this, new ChessEventArgs(chess));
        }

        public void NamePlayersOne(Chess chess)
        {
            _NamePlayersOne?.Invoke(this, new ChessEventArgs(chess));
        }

        public void NamePlayerstTwo(Chess chess)
        {
            _NamePlayersTwo?.Invoke(this, new ChessEventArgs(chess));
        }

        public void DebutChess(Chess chess1, Chess chess2)
        {
            _DebutChess?.Invoke(this, new ChessEventArgs(chess1, chess2));
        }

    }


    public class ChessEventArgs : EventArgs
    {

        public Chess chess;
        public Chess chesstwo;
        public ChessEventArgs(Chess chess)
        {
            this.chess = chess;
        }
        public ChessEventArgs(Chess chess1, Chess chess2)
        {
            chess = chess1;
            chesstwo = chess2;
        }

    }



    public class Program
    {

        static void Main(string[] args)
        {
            Console.Clear();
            List<Chess> white = White();
            List<Chess> black = Black();
            ChessEvent match = new ChessEvent() { White = white, Black = black };


            match._GetDeskWhite += _GetDeskWhite;
            match._GetDeskBlack += _GetDeskBlack;
            match._NamePlayersOne += _NamePlayersOne;
            match._NamePlayersTwo += _NamePlayersTwo;
            match._DebutChess += _DebutChess;

            match.GetDeskWhite(white[0]);
            match.GetDeskBlack(black[2]);
            match.NamePlayersOne(white[0]);
            match.NamePlayerstTwo(black[2]);
            match.DebutChess(white[0], black[2]);

            
        }


        private static void _GetDeskWhite(object sender, ChessEventArgs e)
        {
            Console.WriteLine($"За першою дошкою грає білими {e.chess.Name}");
        }

        private static void _GetDeskBlack(object sender, ChessEventArgs e)
        {
            Console.WriteLine($"За першою дошкою грає чорними {e.chess.Name}");
        }

        private static void _NamePlayersOne(object sender, ChessEventArgs e)
        {
            Console.WriteLine($"{e.chess.Name} перший у світі по рейтингу");
        }

        private static void _NamePlayersTwo(object sender, ChessEventArgs e)
        {
            Console.WriteLine($"{e.chess.Name} п'ятий у світі по рейтингу");
        }

        private static void _DebutChess(object sender, ChessEventArgs e)
        {
            Console.WriteLine($"{e.chess.Name} i {e.chesstwo.Name} розіграли Італійську партію");
        }


        static List<Chess> White()
        {
            List<Chess> white = new List<Chess>();
            white.Add(new Chess()
            {
                Name = "Магнус Карлсен"
            });
            white.Add(new Chess()
            {
                Name = "Василь Іванчук"
            });
            white.Add(new Chess()
            {
                Name = "Вашє Лаграв"
            });
            return white;
        }

        static List<Chess> Black()
        {
            List<Chess> black = new List<Chess>();
            black.Add(new Chess()
            {
                Name = "Левон Аронян"
            });
            black.Add(new Chess()
            {
                Name = "Олександр Грищук"
            });
            black.Add(new Chess()
            {
                Name = "Хікару Накамура"
            });
            return black;
        }
    }
}

