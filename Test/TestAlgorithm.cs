﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace MykolaMartyniuk.RobotChallange.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);

            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });

            var robot = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 3) } };
            var command = algorithm.DoStep(robot, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);

        }
        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);

            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });

            var robot = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) } };
            var command = algorithm.DoStep(robot, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);

        }

        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(2, 4);
            Assert.AreEqual(10, DistanceHelper.FindDistance(p1, p2));
        }


        [TestMethod]

        public void TestIsFree()
        {
            var algorithm = new MykolaMartyniukAlgorithm();

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(2,2), }
            };
            robots.Add(new Robot.Common.Robot() { Energy = 500, Position = new Position(7, 2) });

            Assert.IsTrue(algorithm.IsCellFree(new Position(3, 1), robots[0], robots));
            Assert.IsTrue(algorithm.IsCellFree(new Position(1, 1), robots[0], robots));
        }


        [TestMethod]

        public void TestCreateRobot()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();

            map.Stations.Add(new EnergyStation() { Energy = 700, Position = new Position(3, 3) });
            map.Stations.Add(new EnergyStation() { Energy = 700, Position = new Position(4, 4) });
            map.Stations.Add(new EnergyStation() { Energy = 700, Position = new Position(5, 5) });
            map.Stations.Add(new EnergyStation() { Energy = 700, Position = new Position(6, 6) });

            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot(){Energy = 2000, Position = new Position(1,1) }
            };

            robots.Add(new Robot.Common.Robot()
            { Energy = 2000, Position = new Position(0, 0) });

            var testmethod = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(testmethod is CreateNewRobotCommand);
        }

        [TestMethod]
        public void ChangePosition()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPos = new Position(5, 5);
            var station1 = new EnergyStation()
            {
                Energy = 3454,
                Position = stationPos,
                RecoveryRate = 4
            };
            map.Stations.Add(station1);
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot()
                {
                    Energy = 50, Position = new Position (5, 8)
                }

            };
            var testmethod = algorithm.DoStep(robots, 0, map) as MoveCommand;
            Assert.AreEqual(map.Stations[0].Position, testmethod.NewPosition);

        }

        [TestMethod]

        public void RobotMoveX()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPos = new Position(15, 5);
            var station = new EnergyStation();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 2 });
            var robot = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, Position = new Position(5, 5) } };
            var command = algorithm.DoStep(robot, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition.X, 10);
        }

        [TestMethod]

        public void RobotMoveY()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPos = new Position(15, 15);
            var station = new EnergyStation();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 2 });
            var robot = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, Position = new Position(5, 5) } };
            var command = algorithm.DoStep(robot, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition.Y, 7);
        }


        [TestMethod]
        public void NextStation()
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPos = new Position(5, 5);
            var station = new EnergyStation();
            var stationPos2 = new Position(15, 15);
            map.Stations.Add(new EnergyStation() { Energy = 0, Position = stationPos, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos2, RecoveryRate = 2 });
            var robot = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 100, Position = new Position(5, 5) } };
            var command = algorithm.DoStep(robot, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition.Y, 7);
        }

        [TestMethod]

        public void RobotMoveOut() 
        {
            var algorithm = new MykolaMartyniukAlgorithm();
            var map = new Map();
            var stationPos = new Position(15, 5);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot(){Energy = 100, Position = new Position(5,5) },
                new Robot.Common.Robot(){Energy = 100, Position = new Position(15,5) }
            };


            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is null);
           Assert.AreEqual(robots[0].Position.X, 5);
            Assert.AreEqual(robots[0].Position.Y, 5);

        }
    }
}
