﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MykolaMartyniuk.RobotChallange
{

     public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));

        }
    }



    public class MykolaMartyniukAlgorithm : IRobotAlgorithm
    {

        public MykolaMartyniukAlgorithm()

        {
        }

        public string Author
        {

            get { return "Mykola Martyniuk"; }
        }

        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
        IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots) && station.Energy > 0) 
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
       IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }


        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            

            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > 300) && (robots.Count < map.Stations.Count)) 
            {
                return new CreateNewRobotCommand();
            }
            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            if (stationPosition == null)
                return null;
            if (stationPosition == movingRobot.Position)
                return new CollectEnergyCommand();
            else
            {
                Position p = stationPosition;
                Position pr = movingRobot.Position;
                int possibleEnergy = (int)(movingRobot.Energy * 0.5); 

                int d = DistanceHelper.FindDistance(stationPosition, movingRobot.Position);

                if (d <= possibleEnergy)
                    return new MoveCommand()
                    {
                        NewPosition = p
                    };
                else
                {
                    double coef = (double)possibleEnergy / d;
                 
                    pr.X += (int)((stationPosition.X - movingRobot.Position.X) * coef);
                    pr.Y += (int)((stationPosition.Y - movingRobot.Position.Y) * coef);
                    return new MoveCommand()
                    {
                        NewPosition = pr
                    };
                }
            }
        }

        public LogRoundEventHandler Logger_OnLogRound { get; set; }
    }

}
